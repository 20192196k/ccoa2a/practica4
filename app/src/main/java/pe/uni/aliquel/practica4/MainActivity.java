package pe.uni.aliquel.practica4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;
import android.content.Intent;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridview;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gridview = findViewById(R.id.grid_view_layout);
        fillArray();
        GridAdapter gridAdapter = new GridAdapter(this, text, image);
        gridview.setAdapter(gridAdapter);
        gridview.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(MainActivity.this, SegundoActivity.class);
            intent.putExtra("plato_titulo", text.get(position));
            startActivity(intent);
        }
        );


}
    private void fillArray() {
        text.add("albondiga");
        text.add("arrozconpollo");
        text.add("carne");
        text.add("ceviche");
        text.add("hamburguesa");
        text.add("lazana");
        text.add("pizza");
        text.add("salchipapa");
        image.add(R.drawable.albondiga);
        image.add(R.drawable.arrozconpollo);
        image.add(R.drawable.carne);
        image.add(R.drawable.ceviche);
        image.add(R.drawable.hamburguesa);
        image.add(R.drawable.lazana);
        image.add(R.drawable.pizza);
        image.add(R.drawable.salchipapa);
    }


}
