package pe.uni.aliquel.practica4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SegundoActivity extends AppCompatActivity {
    TextView titulo_edit;
    Spinner spinnercantidad;
    EditText name_destinatorio, dirreccion_destinatario;
    ArrayAdapter<CharSequence> adapter;
    Button button_envio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundo);
        spinnercantidad = findViewById(R.id.spinner_plato);
        titulo_edit = findViewById(R.id.plato_seleccionado);
        name_destinatorio = findViewById(R.id.name_destinatario);
        dirreccion_destinatario = findViewById(R.id.direct_destinatario);
        button_envio = findViewById(R.id.envio_datos);

        Intent intent2 = getIntent();
        String plato_string_titulo = intent2.getStringExtra("plato_titulo");
        titulo_edit.setText(plato_string_titulo);

        adapter = ArrayAdapter.createFromResource(this, R.array.cantidad, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnercantidad.setAdapter(adapter);

        button_envio.setOnClickListener(v->{
           if(name_destinatorio.getText().toString().equals("") || dirreccion_destinatario.getText().toString().equals("")){
               Toast.makeText(SegundoActivity.this, R.string.adv_text_msg, Toast.LENGTH_LONG).show();
        }
        });
    }
}